import '../scss/style.scss';
import {randomShape, play} from './game_logic.js';

const root = document.querySelector('#root');
const userChoice = root.querySelector('.user__choice');
const userChoiceRock = root.querySelector('.user__choice--rock');
const userChoicePaper = root.querySelector('.user__choice--paper');
const userChoiceScissors = root.querySelector('.user__choice--scissors');
const resetBtn = root.querySelector('.reset');
const userScore = root.querySelector('.score__user');
const computerScore = root.querySelector('.score__computer');

let roundsCounter = 0;

function setup(event) {
    if (roundsCounter < 3) {
        roundsCounter++;
        const userChoiceText = event.target.innerHTML;
        const computerChoice = randomShape();
        const resultText = root.querySelector('#result');
        const computerImage = root.querySelector(`.img--${computerChoice.toLowerCase()}`);
        const result =  play(userChoiceText, computerChoice);
        resultText.innerHTML = `Round ${roundsCounter}, ${result.message}`;
        resultText.classList.remove('hidden');
        for (let childNode of root.querySelectorAll('img')) {
            childNode === computerImage ?
            childNode.classList.add('chosen') :
            childNode.classList.remove('chosen');
        }

        if (result.humanWin) {
            userScore.innerHTML = +userScore.innerHTML + 1;
        } else if (!result.humanWin && !result.draw) {
            computerScore.innerHTML = +computerScore.innerHTML + 1;
        }

        if (roundsCounter === 3) {
            userChoice.classList.add('hidden');
            resetBtn.classList.remove('hidden');
            if (userScore.innerHTML !== computerScore.innerHTML) {
                resultText.innerHTML += ` GAME OVER: ${userScore.innerHTML > computerScore.innerHTML ? 'You are' : 'Computer is'} a winner!`;
            } else {
                resultText.innerHTML += ` GAME OVER: DRAW!`;
            }
        }
    }
}

userChoiceRock.onclick = event => {
    setup(event);
};

userChoicePaper.onclick = event => {
    setup(event);
};

userChoiceScissors.onclick = event => {
    setup(event);
};

resetBtn.addEventListener('click', () => {
    roundsCounter = 0;
    userScore.innerHTML = 0;
    computerScore.innerHTML = 0;
    for (let childNode of root.querySelectorAll('img')) {
        childNode.classList.remove('chosen');
    }
    userChoice.classList.remove('hidden');
    resetBtn.classList.add('hidden');
});
