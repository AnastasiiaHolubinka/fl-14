const SHAPES = ['Rock', 'Scissors', 'Paper'];
const randomShape = () => SHAPES[Math.floor(Math.random() * SHAPES.length)];

function play(humanChoice, computerChoice = randomShape()) {
    let humanWin = false;
    let draw = false;
    if (humanChoice === 'Rock' && computerChoice === 'Scissors' ||
        humanChoice === 'Scissors' && computerChoice === 'Paper' ||
        humanChoice === 'Paper' && computerChoice === 'Rock') {
            humanWin = true;
    } else if (humanChoice === computerChoice) {
        draw = true;
    }

    const message = draw ? 'Draw.' : `${humanChoice} vs. ${computerChoice}, You've ${humanWin ? 'WON' : 'LOST'}!`;

    return {draw, humanWin, message};
}

export {randomShape, play};
