while (true){
  const userInput = prompt('Enter expression ')
  const expression = userInput.split(' ').join('')
  try {
    const result = eval(expression)
    if (result === undefined ||isNaN(result)) {
      alert('Invalid Input!')
    } else {
      alert(`${expression} = ${result}`)
    }
  } catch (error) {
    alert('Invalid Input!')
  }
}
