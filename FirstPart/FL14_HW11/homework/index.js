//Task 1
function isEquals(value1, value2) {
    return value1 === value2;
}

//Task 2
function numberToString(value) {
    return String(value);
 }
//Task 3
function storeNames(...str) {
    return str;
}
//Task 4
function getDivision(value1, value2 ) {
    return value1 > value2 ? value1 / value2 : value2 / value1;
 }
//Task 5
function negativeCount(value) {
    let count=0 ;
    for (let i = 0; i < value.length; i++) {
            if (value[i] < 0) {
                count++;
            }
    }
    return count;
}
//Task 6
function letterCount(str, letter) {
    let Count = 0;
    for (let position = 0; position < str.length; position++) {
        if (str.charAt(position) === letter) {
            Count += 1;
        }
    }
    return Count;
}

