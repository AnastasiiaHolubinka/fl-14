const questions = JSON.parse(localStorage.getItem('questions'));
const headElem = document.getElementById('head');
const buttonsElem = document.getElementById('buttons');
const pagesElem = document.getElementById('pages');
class Quiz{
	constructor(type, questions, results){
	this.type = type;
	this.questions = questions;
	this.results = results;
	this.score = 0;
	this.result = 0;
	this.current = 0;
	}
	click(index){
  let value = this.questions[this.current].click(index);
	this.score += value;
	let correct = -1;
	if(value >= 1){
			correct = index;
		}else{
			for(let i = 0; i < this.questions[this.current].answers.length; i++){
				if(this.questions[this.current].answers[i].value >= 1){
					correct = i;
					break;
				}
			}
		}
		this.next();
		return correct;
	}
	next(){
		this.current++;
		if(this.current >= this.questions.length){
			this.end();
		}
	}
	end(){
		for(let i = 0; i < this.results.length; i++){
			if(this.results[i].check(this.score)){
				this.result = i;
			}
		}
	}
}
class Question{
	constructor(text, answers){
		this.text = text;
		this.answers = answers;
	}
	click(index){
		return this.answers[index].value;
	}
}
class Answer{
	constructor(text, value){
		this.text = text;
		this.value = value;
	}
}
class Result{
	constructor(text, value){
		this.text = text;
		this.value = value;
	}
	сheck(value){
		if(this.value <= value){
			return true;
		}else{
			return false;
		}
	}
}
const results =
[
	new Result('YOU LOSE', 0)];
const quiz = new Quiz(1, questions, results);
update();
function update(){
	if(quiz.current < quiz.questions.length){
		headElem.innerHTML = quiz.questions[quiz.current].text;
		buttonsElem.innerHTML = '';
		for(let i = 0; i < quiz.questions[quiz.current].answers.length; i++){
			let btn = document.createElement('button');
			btn.className = 'button';
			btn.innerHTML = quiz.questions[quiz.current].answers[i].text;
			btn.setAttribute('index', i);
			buttonsElem.appendChild(btn);
		}
		pagesElem.innerHTML = quiz.current + 1 + ' / ' + quiz.questions.length;
		init();
	}else{
		buttonsElem.innerHTML = '';
		headElem.innerHTML = quiz.results[quiz.result].text;
		pagesElem.innerHTML = 'Очки: ' + quiz.score;
	}
}
function init(){
	let btns = document.getElementsByClassName('button');
	for(let i = 0; i < btns.length; i++){
	btns[i].addEventListener('click', function (e) {
  click(e.target.getAttribute('index'));
  });
	}
}
function click(index){
  let correct = quiz.click(index);
	let btns = document.getElementsByClassName('button');
	for(let i = 0; i < btns.length; i++){
		btns[i].className = 'button button_passive';
	}

	if(quiz.type === 1){
		if(correct >= 0){
			btns[correct].className = 'button button_correct';
		}
		if(index !== correct){
			btns[index].className = 'button button_wrong';
		}
	}else{
		btns[index].className = 'button button_correct';
	}
	setTimeout(update, 1000);
}
