//Task 1
function getAge(date1, date2) {
    date2 = date2 || new Date();
    let difference = date2.getTime() - date1.getTime();
    return Math.floor(difference / (1000 * 60 * 60 * 24 * 365.25));
}
//Task 2
function getWeekDay(date_obj){
    return new Date(date_obj).toLocaleString('en-EN', { weekday: 'long' });
}
//Task 3
function getProgrammersDay(year){
    let programmersDay = 256;
    let tmp_date = new Date(year, 0, 0);
    tmp_date.setDate(tmp_date.getDate()+programmersDay);

    return tmp_date.getDate() +' '
    + tmp_date.toLocaleString('en-EN', {month: 'short'}) + ', '
    +tmp_date.getFullYear()
    +' ('+getWeekDay(tmp_date)+')';
}
//Task 4
function howFarIs(day) {
    day = day[0].toUpperCase() + day.slice(1).toLowerCase();
    const Days = {
        'Monday': 1,
        'Tuesday': 2,
        'Wednesday': 3,
        'Thursday': 4,
        'Friday': 5,
        'Saturday': 6,
        'Sunday': 7
    }
    let today = new Date().getDay();
    if (today === Days[day]) {
        return `Hey, today is ${day} =)`;
    }
    if (today < Days[day]) {
        return `It's ${Days[day] - today} day(s) left till ${day}.`;
    }
    if (today > Days[day]) {
        return `It's ${Days['Sunday'] - (today - Days[day])} day(s) left till ${day}.`;
    }
}
//Task 5
function isValidIdentifier(str) {
    let reg = /^[^0-9\s][0-9a-zA-z_$]*$/
    return reg.test(str);
}
//Task 6
let capitalize = str => str.toLowerCase().replace(/\b\w/g, newStr => newStr[0].toUpperCase() + newStr.slice(1));
//Task 7
function isValidAudioFile(str) {
    let reg = /^([a-zA-Z])+(.mp3|.alac|.aac|.flac)$/
    return reg.test(str);
}
//Task 8
function getHexadecimalColors(str) {
    let reg = /#([a-f0-9]{3}){1,2}\b/gi;
    return reg.test(str);
}
//Task 9
function isValidPassword(str) {
    let reg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g
    return reg.test(str);
}
//Task 10
let addThousandsSeparators = num => num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
