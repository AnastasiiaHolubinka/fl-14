//Task1
const suit = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];
const rank = ['ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King'];

class Card {
    constructor(suit, rank) {
        this.suit = suit;
        this.rank = rank;
        this.isFacedCard = this.rank[0] || this.rank[10] || this.rank[11] || this.rank[12];
    }

    _toString() {
        return `${this.rank} of ${this.suit}`
    }

    static compare(cardOne, cardTwo) {
        if (cardOne.rank === rank[0]) {
            return true;
        } else if (cardTwo.rank === rank[0]) {
            return false;
        } else if (rank.indexOf(cardOne.rank) > rank.indexOf(cardTwo.rank)) {
            return true;
        } else if (rank.indexOf(cardOne.rank) < rank.indexOf(cardTwo.rank)) {
            return false;
        } else {
            return 'draw';
        }
    }
}

class Deck {
    constructor() {
        this.deck = [];
        this.count = 52;
    }

    generateDeck(suit, rank) {
        for (let suits in suit) {
            for (let ranks in rank) {
                this.deck.push(new Card(suit[suits], rank[ranks]));
            }
        }
        return this.deck;
    }
    shuffle() {
        let fullDeck = this.deck.length, temp, i;
        while (fullDeck) {
            i = Math.floor(Math.random() * fullDeck--);
            temp = this.deck[fullDeck];
            this.deck[fullDeck] = this.deck[i]
            this.deck[i] = temp;
        }
        return this.deck;
    }
    draw(n) {
        let chosenCard = [];
        for (let i = n; i > 0; i--) {
            chosenCard.push(this.deck.pop());
            this.count--;
        }
        return chosenCard;
    }
}

class Player {
    constructor(name, wins, deck) {
        this.name = name;
        this.wins = wins;
        this.deck = deck
    }

    static play(PlayerOne, PlayerTwo) {
        PlayerOne.deck.generateDeck(suit, rank);
        PlayerOne.deck.shuffle();
        PlayerTwo.deck.generateDeck(suit, rank);
        PlayerTwo.deck.shuffle();

        for (let i = PlayerOne.deck.count - 1; i >= 0; i--) {
            if (Card.compare(PlayerOne.deck.deck[i], PlayerTwo.deck.deck[i]) === 'draw') {
                continue;
            } else if (Card.compare(PlayerOne.deck.deck[i], PlayerTwo.deck.deck[i])) {
                PlayerOne.wins++
            } else {
                PlayerTwo.wins++
            }

            console.log(PlayerOne.deck.deck[i]._toString());
            console.log(PlayerTwo.deck.deck[i]._toString());
            PlayerOne.deck.draw(1);
            PlayerTwo.deck.draw(1);
            console.log('1', PlayerOne.wins);
            console.log('2', PlayerTwo.wins);
        }

        if (PlayerOne.wins > PlayerTwo.wins) {
            console.log(`${PlayerOne.name} wins ${PlayerOne.wins} to ${PlayerTwo.wins}`);
        } else if (PlayerOne.wins < PlayerTwo.wins) {
            console.log(`${PlayerTwo.name} wins ${PlayerTwo.wins} to ${PlayerOne.wins}`);
        } else {
            console.log('The score is equal')
        }

    }
}

let deck1 = new Deck();
let deck2 = new Deck();

let player1 = new Player('Oleh', 0, deck1);
let player2 = new Player('Anna', 0, deck2);

Player.play(player1, player2);
// Task2
class Employee {
    constructor({ id, firstName, lastName, birthday, salary, position, department }) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.salary = salary;
        this.position = position;
        this.department = department;
        Employee.EMPLOYEES.push(this);

    }

    static get EMPLOYEES() {
        if (!this._EMPLOYEES) {
            this._EMPLOYEES = [];
        }
        return this._EMPLOYEES;
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    get age() {
        let todayDate = new Date();
        let bDayDate = new Date(this.birthday);
        return todayDate.getFullYear() - bDayDate.getFullYear();
    }

    quit() {
        if (Employee.EMPLOYEES.includes(this)) {
            Employee.EMPLOYEES.splice(Employee.EMPLOYEES.indexOf(this), 1);
        }
    }

    retire() {
        this.quit();
        console.log('It was such a pleasure to work with you!');
    }

    getFired() {
        this.quit();
        console.log('Not a big deal!');
    }

    changeDepartment(newDepartment) {
        this.department = newDepartment;
    }

    changePosition(newPosition) {
        this.position = newPosition;
    }

    changeSalary(newSalary) {
        this.salary = newSalary;
    }

    getPromoted(benefits) {
        if (benefits.salary) {
            this.salary += benefits.salary;
        } else if (benefits.position) {
            this.position = benefits.position;
        } else if (benefits.department) {
            this.department = benefits.department;
        }
        console.log('Yoohooo!');
    }

    getDemoted(punishment) {
        if (punishment.salary) {
            this.salary -= punishment.salary;
        } else if (punishment.position) {
            this.position = punishment.position;
        } else if (punishment.department) {
            this.department = punishment.department;
        }
        console.log('Damn!');
    }

}

class Manager extends Employee {

    constructor(id, firstName, lastName, birthday, salary, department) {
        super(id, firstName, lastName, birthday, salary, department);
        this.position = 'manager';
    }


    get managedEmployees() {

        return Employee.EMPLOYEES.filter(employee => {
            return employee.position !== 'manager' && employee.department === this.department;
        });
    }
}

class BlueCollarWorker extends Employee { }

class HRManager extends Manager {

    constructor(id, firstName, lastName, birthday, salary) {
        super(id, firstName, lastName, birthday, salary);
        this.department = 'hr';
    }

}

class SalesManager extends Manager {
    constructor(id, firstName, lastName, birthday, salary) {
        super(id, firstName, lastName, birthday, salary);
        this.department = 'sales';
    }

}

//Task3
const promoter = (managerPro) => ({
    promote: (index, benefit) => {
        managerPro.managedEmployees.find((employee) => employee.id === index).getPromoted({ salary: benefit });
    }
});

function managerPro(managerPro) {
    return Object.assign(managerPro, promoter(managerPro));
}

//test cases
const salesManager = new SalesManager({
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    birthday: '10/04/1994',
    salary: 5000
});

const hrManager = new HRManager({
    id: 2,
    firstName: 'Bob',
    lastName: 'Doe',
    birthday: '10/04/1994',
    salary: 5000
});

const blueCollarWorkerOne = new BlueCollarWorker({
    id: 3,
    firstName: 'Mary',
    lastName: 'Doe',
    birthday: '10/04/1994',
    salary: 5000,
    position: 'office worker',
    department: 'sales'
});

const blueCollarWorkerTwo = new BlueCollarWorker({
    id: 4,
    firstName: 'Jane',
    lastName: 'Doe',
    birthday: '10/04/1994',
    salary: 5000,
    position: 'office worker',
    department: 'hr'
});

console.log(Employee.EMPLOYEES);

console.log(salesManager.getPromoted({ salary: 7500 }));
console.log(salesManager);

console.log(blueCollarWorkerTwo.birthday);
console.log(blueCollarWorkerTwo.fullName);
console.log(blueCollarWorkerTwo.age);

const ManagerPro = managerPro(salesManager);
console.log(ManagerPro.promote(3, 6000));
console.log(blueCollarWorkerTwo);
console.log(blueCollarWorkerOne);
console.log(blueCollarWorkerOne.getFired());

console.log(Employee.EMPLOYEES);
